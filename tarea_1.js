// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: 
//    $ mongoimport -d students -c grades < grades.json 

/*

    -- Terminal --
        $ cd <directorio_con_archivo_json> 
        $ mongoimport --db dbName --collection collectionName < grades.json 

    -- Respuesta --
        2022-11-07T07:52:00.381-0600	connected to: mongodb://localhost/
        2022-11-07T07:52:00.553-0600	800 document(s) imported successfully. 0 document(s) failed to import.

*/

// 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. 
// Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: 
//  >use students; 
//  >db.grades.count() 
//  ¿Cuántos registros arrojo el comando count?

/*

    -- STUDIO 3T --
        $ db.collectionName.count({});

    -- Respuesta --
        800

*/

// 3) Encuentra todas las calificaciones del estudiante con el id numero 4.

/*

    -- STUDIO 3T --
        $ db.collectionName.aggregate([
            {
                $match:{
                    "student_id": 4
                }
            },
            {
                $project:{
                    "_id": 1,
                    "type":1,
                    "score":1
                }
            }
        ]);

    -- Respuesta --
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb587"),
            "type" : "exam",
            "score" : 87.89071881934647
        }
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb589"),
            "type" : "homework",
            "score" : 5.244452510818443
        }
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb58a"),
            "type" : "homework",
            "score" : 28.656451042441
        }
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb588"),
            "type" : "quiz",
            "score" : 27.29006335059361
        }
*/

// 4) ¿Cuántos registros hay de tipo exam?

/*

    -- STUDIO 3T --
        $ db.collectionName.count({"type" : "exam"})

    -- Respuesta --
        200

*/

// 5) ¿Cuántos registros hay de tipo homework?

/*

    -- STUDIO 3T --
        $ db.collectionName.count({"type" : "homework"})

    -- Respuesta --
        400

*/

// 6) ¿Cuántos registros hay de tipo quiz?

/*

    -- STUDIO 3T --
        $ db.collectionName.count({"type" : "quiz"})

    -- Respuesta --
        200

*/

// 7) Elimina todas las calificaciones del estudiante con el id numero 3

/*

    -- STUDIO 3T --
        $ db.collectionName.updateMany({"student_id":3},{$unset: {"score": ""}});
        $ db.collectionName.find({"student_id":3});

    -- Respuesta --
        {
            "acknowledged" : true,
            "matchedCount" : 4.0,
            "modifiedCount" : 4.0
        }

        {
            "_id" : ObjectId("50906d7fa3c412bb040eb583"),
            "student_id" : NumberInt(3),
            "type" : "exam"
        }
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb584"),
            "student_id" : NumberInt(3),
            "type" : "quiz"
        }
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb585"),
            "student_id" : NumberInt(3),
            "type" : "homework"
        }
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb586"),
            "student_id" : NumberInt(3),
            "type" : "homework"
        }


*/

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

/*

    -- STUDIO 3T --
        $ db.collectionName.aggregate([
            {
                $match:{
                    $and:[{"type" : "homework"},{"score":75.29561445722392}]
                }
            }
        ])

    -- Respuesta --
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb59e"),
            "student_id" : NumberInt(9),
            "type" : "homework",
            "score" : 75.29561445722392
        }

*/

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

/*

    -- STUDIO 3T --
        $ db.collectionName.updateMany({"_id":ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":10}});
        $ db.collectionName.find({_id:ObjectId("50906d7fa3c412bb040eb591")});

    -- Respuesta --
        // FIRST
        {
            "acknowledged" : true,
            "matchedCount" : 1.0,
            "modifiedCount" : 1.0
        }
        // SECOND
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb591"),
            "student_id" : NumberInt(6),
            "type" : "homework",
            "score" : 10.0
        }

*/

// 10) A qué estudiante pertenece esta calificación.

/*

    -- STUDIO 3T --
        $ db.collectionName.aggregate([
            {
                $match:{
                    "_id":ObjectId("50906d7fa3c412bb040eb591")
                }
            },
            {
                $project:{
                    "student_id": 1
                }
            }
        ]);

    -- Respuesta --
        {
            "_id" : ObjectId("50906d7fa3c412bb040eb591"),
            "student_id" : NumberInt(6)
        }

*/